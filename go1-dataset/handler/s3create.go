package handler

import (
	"bufio"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func printout(event events.S3Event) {
	data, _ := json.Marshal(event)

	// Now convert to a string and output
	// Cloudwatch picks up the json and formats it nicely for us. :)
	strEventInfo := string(data)
	fmt.Printf("S3 Event: %s\n", strEventInfo)
}

// S3EventHandle handle s3 event
func S3EventHandle(ctx context.Context, s3Event events.S3Event) error {
	// printout(s3Event)

	// try to process first record
	if len(s3Event.Records) == 0 {
		return nil
	}

	for _, event := range s3Event.Records {
		err := handleRecord(event)
		if err != nil {
			return err
		}
	}

	return nil
}

func handleRecord(record events.S3EventRecord) error {
	sess, err := session.NewSession()
	if err != nil {
		return err
	}

	s3Service := s3.New(sess, &aws.Config{Region: aws.String("ap-southeast-1")})
	resp, err := s3Service.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(record.S3.Bucket.Name),
		Key:    aws.String(record.S3.Object.Key),
	})
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	csvReader := csv.NewReader(bufio.NewReaderSize(resp.Body, 1<<10)) // read with buffer 1kb
	csvReader.LazyQuotes = true
	csvReader.TrimLeadingSpace = true

	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}

		// print raw data to cloudwatch
		fmt.Println(line)
	}

	return nil
}
